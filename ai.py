import torch
from transformers import GPT2LMHeadModel, GPT2Tokenizer
from transformers import Trainer, TrainingArguments
from datasets import load_dataset

# Load pre-trained model and tokenizer
model_name = "distilgpt2"
tokenizer = GPT2Tokenizer.from_pretrained(model_name)
tokenizer.pad_token = tokenizer.eos_token
model = GPT2LMHeadModel.from_pretrained(model_name)

# Load and tokenize the dataset from the "data" folder
dataset = load_dataset("text", data_files={"train": "data/*.txt"})


def tokenize_function(example):
    input_tokens = tokenizer(example["text"], return_tensors="pt",
                             padding="max_length", truncation=True, max_length=128)
    input_tokens["labels"] = input_tokens["input_ids"].detach().clone()
    return input_tokens


tokenized_dataset = dataset.map(tokenize_function, batched=True)
train_dataset = tokenized_dataset["train"]

# Set up the training arguments
training_args = TrainingArguments(
    output_dir="./output",
    overwrite_output_dir=True,
    num_train_epochs=5,
    per_device_train_batch_size=8,
    save_steps=50_0,
    save_total_limit=2,
    logging_steps=50,
    # resume training from the checkpoint saved at step 500
    resume_from_checkpoint="./output/checkpoint-last",
)

# Train the model
trainer = Trainer(
    model=model,
    args=training_args,
    train_dataset=train_dataset,
)

trainer.train()

# Save the fine-tuned model
trainer.save_model("./fine_tuned_model")
