import torch
from transformers import GPT2LMHeadModel, GPT2Tokenizer

def load_model(model_path: str, model_name: str = "distilgpt2"):
    tokenizer = GPT2Tokenizer.from_pretrained(model_name)
    model = GPT2LMHeadModel.from_pretrained(model_path)
    return tokenizer, model

def generate_text(tokenizer, model, input_text: str, max_length: int = 200):
    input_ids = tokenizer.encode(input_text, return_tensors="pt")
    attention_mask = torch.ones_like(input_ids)
    output = model.generate(
        input_ids,
        max_length=max_length,
        min_length=50,  # Set a minimum length for the generated text
        num_return_sequences=1,
        attention_mask=attention_mask,
        do_sample=True,  # Enable sampling to generate more diverse text
        temperature=0.8,  # Adjust the temperature to control randomness (higher values = more random)
    )
    decoded_output = tokenizer.decode(output[0], skip_special_tokens=True)
    return decoded_output


if __name__ == "__main__":
    # Load the fine-tuned model
    model_path = "./fine_tuned_model"
    tokenizer, model = load_model(model_path)

    while True:
        # Get input from the user
        input_text = input("Enter input text: ")

        # Generate text using the fine-tuned model
        decoded_output = generate_text(tokenizer, model, input_text)
        print("Generated text: ", decoded_output)
